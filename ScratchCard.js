import ScratchView from 'react-native-scratch';
import React, {Component} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import styles from './style';
// const Sound = require('react-native-sound');
import SoundPlayer from 'react-native-sound-player';
// import Icon from 'react-native-vector-icons/FontAwesome';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Grid,
  Col,
  Row,
  Icon,
  Footer,
  FooterTab,
  Button,
} from 'native-base';

export default class ScratchCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cards1: [],
      cards2: [],
      cards3: [],
    };
    console.disableYellowBox = true;
  }
  componentWillMount() {
    const itemes = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    let cards1 = [];
    let cards2 = [];
    let cards3 = [];
    itemes.forEach(item => {
      let name = card + item;

      let card = {};
      card['card' + item] = item;
      if (item <= 3) {
        card.text = 'Congrats You Won';
        cards1.push(card);
      }
      if (item > 3 && item <= 6) {
        card.text = 'Congrats You Won';
        cards2.push(card);
      }
      if (item > 6) {
        card.text = 'Congrats You Won';
        cards3.push(card);
      }
    });
    this.setState({
      cards1: cards1,
      cards2: cards2,
      cards3: cards3,
    });
  }

  onImageLoadFinished = ({id, success}) => {
    // Do something
  };

  onScratchProgressChanged = async ({value, id}) => {
    console.log('onScratchProgressChanged');
    try {
      // play the file tone.mp3
      SoundPlayer.playSoundFile('scratch', 'mp3');
      console.log('play');
      // or play from url  console.log(`cannot play the sound file`, e);
      // SoundPlayer.playUrl('https://example.com/music.mp3');
    } catch (e) {
      console.log(`cannot play the sound file`, e);
    }

    // Do domething like showing the progress to the user
  };

  onScratchDone = ({isScratchDone, id}) => {
    // Do something
  };

  onScratchTouchStateChanged = ({id, touchState}) => {
    // Example: change a state value to stop a containing
    // FlatList from scrolling while scratching
    this.setState({scrollEnabled: !touchState});
  };
  render() {
    return (
      <Container>
        <Header hasTabs style={{backgroundColor: '#fff'}}>
          <Left>
            <TouchableOpacity>
              <Icon size={28} style={{color: '#000'}} name="md-menu" />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title style={styles.HeaderTitle}>Scratch Card</Title>
          </Body>
          <Right></Right>
        </Header>

        <Content style={{padding: 20, marginTop: 70}}>
          <Grid
            style={{
              backgroundColor: 'red',
            }}>
            <View
              style={{
                position: 'absolute',
                width: 50,
                height: 50,
                borderRadius: 200,
                left: -26,
                top: -27,

                backgroundColor: 'white',
              }}
            />
            <View
              style={{
                position: 'absolute',
                width: 50,
                height: 50,
                borderRadius: 200,
                left: -26,
                bottom: -27,
                backgroundColor: 'white',
              }}
            />
            <View
              style={{
                position: 'absolute',
                right: -26,
                width: 50,
                height: 50,
                borderRadius: 25,
                top: -27,
                backgroundColor: 'white',
              }}
            />
            <View
              style={{
                position: 'absolute',
                right: -26,
                width: 50,
                height: 50,
                borderRadius: 25,
                bottom: -27,
                backgroundColor: 'white',
              }}
            />
            <Col style={{marginBottom: 15, marginTop: 15, marginLeft: 15}}>
              {this.state.cards1.map((value, key) => {
                return (
                  <Row
                    style={{
                      height: 90,
                      margin: 1,
                      backgroundColor: 'white',
                    }}>
                    <Text
                      style={{
                        marginTop: 25,
                        fontWeight: 'bold',
                        textAlign: 'center',
                      }}>
                      {value.text}
                    </Text>
                    <ScratchView
                      id={key} // ScratchView id (Optional)
                      brushSize={40} // Default is 10% of the smallest dimension (width/height)
                      threshold={70} // Report full scratch after 70 percentage, change as you see fit. Default is 50
                      fadeOut={false} // Disable the fade out animation when scratch is done. Default is true
                      placeholderColor="AAAAAA" // Scratch color while image is loading (or while image not present)
                      imageUrl="https://target.scene7.com/is/image/Target/GUEST_865798d5-8236-4360-8bee-77690e99178f?fmt=webp&wid=1400&qlt=80" // A url to your image (Optional)
                      resourceName="your_image" // An image resource name (without the extension like '.png/jpg etc') in the native bundle of the app (drawble for Android, Images.xcassets in iOS) (Optional)
                      resizeMode="cover|contain|stretch" // Resize the image to fit or fill the scratch view. Default is stretch
                      onImageLoadFinished={this.onImageLoadFinished} // Event to indicate that the image has done loading
                      onTouchStateChanged={this.onTouchStateChangedMethod} // Touch event (to stop a containing FlatList for example)
                      onScratchProgressChanged={this.onScratchProgressChanged} // Scratch progress event while scratching
                      onScratchDone={this.onScratchDone} // Scratch is done event
                    />
                  </Row>
                );
              })}
            </Col>
            <Col style={{marginBottom: 15, marginTop: 15}}>
              {this.state.cards1.map((value, key) => {
                return (
                  <Row
                    style={{height: 90, margin: 1, backgroundColor: 'white'}}>
                    <Text
                      style={{
                        marginTop: 25,
                        fontWeight: 'bold',
                        textAlign: 'center',
                      }}>
                      {value.text}
                    </Text>
                    <ScratchView
                      id={key} // ScratchView id (Optional)
                      brushSize={40} // Default is 10% of the smallest dimension (width/height)
                      threshold={70} // Report full scratch after 70 percentage, change as you see fit. Default is 50
                      fadeOut={false} // Disable the fade out animation when scratch is done. Default is true
                      placeholderColor="AAAAAA" // Scratch color while image is loading (or while image not present)
                      imageUrl="https://target.scene7.com/is/image/Target/GUEST_865798d5-8236-4360-8bee-77690e99178f?fmt=webp&wid=1400&qlt=80" // A url to your image (Optional)
                      resourceName="your_image" // An image resource name (without the extension like '.png/jpg etc') in the native bundle of the app (drawble for Android, Images.xcassets in iOS) (Optional)
                      resizeMode="cover|contain|stretch" // Resize the image to fit or fill the scratch view. Default is stretch
                      onImageLoadFinished={this.onImageLoadFinished} // Event to indicate that the image has done loading
                      onTouchStateChanged={this.onTouchStateChangedMethod} // Touch event (to stop a containing FlatList for example)
                      onScratchProgressChanged={this.onScratchProgressChanged} // Scratch progress event while scratching
                      onScratchDone={this.onScratchDone} // Scratch is done event
                      style={{margin: 16}}
                    />
                  </Row>
                );
              })}
            </Col>
            <Col style={{marginBottom: 15, marginTop: 15, marginRight: 15}}>
              {this.state.cards1.map((value, key) => {
                return (
                  <Row
                    style={{height: 90, margin: 1, backgroundColor: 'white'}}>
                    <Text
                      style={{
                        marginTop: 25,
                        fontWeight: 'bold',
                        textAlign: 'center',
                      }}>
                      {value.text}
                    </Text>
                    <ScratchView
                      id={key} // ScratchView id (Optional)
                      brushSize={40} // Default is 10% of the smallest dimension (width/height)
                      threshold={70} // Report full scratch after 70 percentage, change as you see fit. Default is 50
                      fadeOut={false} // Disable the fade out animation when scratch is done. Default is true
                      placeholderColor="AAAAAA" // Scratch color while image is loading (or while image not present)
                      imageUrl="https://target.scene7.com/is/image/Target/GUEST_865798d5-8236-4360-8bee-77690e99178f?fmt=webp&wid=1400&qlt=80" // A url to your image (Optional)
                      resourceName="your_image" // An image resource name (without the extension like '.png/jpg etc') in the native bundle of the app (drawble for Android, Images.xcassets in iOS) (Optional)
                      resizeMode="cover|contain|stretch" // Resize the image to fit or fill the scratch view. Default is stretch
                      onImageLoadFinished={this.onImageLoadFinished} // Event to indicate that the image has done loading
                      onTouchStateChanged={this.onTouchStateChangedMethod} // Touch event (to stop a containing FlatList for example)
                      onScratchProgressChanged={this.onScratchProgressChanged} // Scratch progress event while scratching
                      onScratchDone={this.onScratchDone} // Scratch is done event
                    />
                  </Row>
                );
              })}
            </Col>
          </Grid>
        </Content>
        <Footer>
          <FooterTab style={{backgroundColor: '#393377'}}>
            <Button
              vertical
              // active={props.navigationState.index === 0}
              // onPress={() => props.navigation.navigate("LucyChat")}
            >
              <Icon name="home" />
              <Text>Lucy</Text>
            </Button>
            <Button
              vertical
              // active={props.navigationState.index === 0}
              // onPress={() => props.navigation.navigate("LucyChat")}
            >
              <Icon name="bowtie" />
              <Text>Lucy</Text>
            </Button>

            <View
              style={{
                // position: 'absolute',
                // right: 154,
                width: 70,
                height: 70,
                borderRadius: 70,
                bottom: 35,
                justifyContent: 'center',
                backgroundColor: '#393377',
              }}></View>
            <Button
              vertical
              style={{
                position: 'absolute',
                right: 150,
                width: 60,
                height: 60,
                borderRadius: 60,
                bottom: 25,

                backgroundColor: 'yellow',
              }}
              // active={props.navigationState.index === 1}
              // onPress={() => props.navigation.navigate("JadeChat")}
            >
              <Icon name="briefcase" />
              {/* <Text>Nine</Text> */}
            </Button>
            <Button
              vertical
              // active={props.navigationState.index === 0}
              // onPress={() => props.navigation.navigate("LucyChat")}
            >
              <Icon name="bowtie" />
              <Text>Lucy</Text>
            </Button>
            <Button
              vertical
              // active={props.navigationState.index === 2}
              // onPress={() => props.navigation.navigate("NineChat")}
            >
              <Icon name="headset" />
              <Text>Jade</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

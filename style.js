import {Dimensions, StyleSheet} from 'react-native';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import * as colors from '../../../constants/colors';
const window = Dimensions.get('window');
const styles = StyleSheet.create({
  HeaderTitle: {
    color: 'red',
    width: 230,
    fontSize: 18,
    fontWeight: 'bold',
  },

  scheduleTextInput: {
    borderWidth: 1,
    borderColor: 'blue',
    height: '60%',
    width: '100%',
    borderRadius: -50,
    marginTop: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },

  btn: {
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 40,
    borderColor: 'blue',
    borderWidth: 1,
    marginLeft: 70,
    marginRight: 70,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 10,
    marginBottom: 10,
  },

  btnTxt: {
    color: 'blue',
    fontSize: 22,
    textAlign: 'center',
    alignContent: 'center',
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },

  Textstyle: {
    fontSize: 16,
    marginTop: '15%',
  },

  feetStyle: {
    fontSize: 16,
    marginTop: '15%',
    marginLeft: '5%',
  },
});

export default styles;
